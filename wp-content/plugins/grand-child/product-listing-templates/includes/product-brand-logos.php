<?php
$collection =  $meta_values['collection'][0];
$storename = do_shortcode('[Retailer "companyname"]');
$alt = "Floorte waterproof hardwood flooring for home | ".$storename;
$brand = trim($meta_values['brand_facet'][0]);
if ( $brand == 'Shaw Floors') { 
	if($collection == 'Floorte Exquisite' || $collection == 'Floorte Magnificent'){	?>
		<span class="floorte_brand">SHAW FLOORS</span>
		<img class="floorte_brandlogo" src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/floorte_waterproof.png" alt="floorte" class="product-logo" />
 <?php }else{
	?>
	<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/shaw_logo.png" alt="Shaw Floors" class="product-logo" />
<?php } } elseif ($brand == 'Anderson Tuftex') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/anderson_tuftex_logo.png" alt="Anderson Tuftex" class="product-logo" />
<?php } elseif ($brand == 'Karastan'){ ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/karastan_logo.png" alt="Karastan" class="product-logo" />
<?php }elseif ($brand == 'Armstrong') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/armstrong_logo.png" alt="Armstrong" class="product-logo" />
<?php } elseif ($brand == 'Dream Weaver') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/dreamweaver_logo.png" alt="Dream Weaver" class="product-logo" />
<?php }elseif ($brand == 'Philadelphia Commercial') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/philadelphia_commercial_logo.png" alt="Philadelphia Commercial" class="product-logo" />
<?php }elseif ($brand == 'COREtec') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/coretec_logo.png" alt="COREtec" class="product-logo" />
<?php }  elseif ($brand == 'Daltile') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/daltile_logo.png" alt="Daltile" class="product-logo" />
<?php } elseif ($brand == 'Floorscapes') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/floorscapes.jpg" alt="Floorscapes" class="product-logo" />
<?php }  elseif ($brand == 'Mohawk') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/mohawk_logo.png" alt="Mohawk" class="product-logo" />
<?php } elseif ($brand == 'Bruce') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/bruce_logo.png" alt="Bruce" class="product-logo" />
<?php } elseif ($brand == 'American Olean') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/amrican_olean_logo.jpg" alt="American Olean" class="product-logo" />
<?php } elseif ($brand == 'Mannington') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/mannington_logo.png" alt="Mannington" class="product-logo" />
<?php } elseif ($brand == 'Fabrica') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/fabrica.png" alt="Fabrica" class="product-logo" />
<?php } elseif ($brand == 'Masland') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/mas-land.png" alt="Masland" class="product-logo" />
<?php } elseif ($brand == 'Dixie Home') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/dixie-home.png" alt="Dixie Home" class="product-logo" />
<?php } elseif ($brand == 'Nourison') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/nourison_logo.png" alt="Nourison" class="product-logo" />		
<?php }elseif ($brand == 'Robbins') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/robbins.png" alt="Robbins" class="product-logo" />		
<?php }	else { ?> 
    <?php echo $brand ; ?>
<?php } ?>